$(function() {
    // $("#sidebar").mCustomScrollbar({
    //     theme: "minimal"
    // });

    $('#dismissSideBar, .overlaySideBar').on('click', function () {
        $('#sidebarMenu').removeClass('active');
        $('.overlaySideBar').removeClass('active');
        $('body').css("overflow", "auto");
    });

    $('#sidebarCollapse').on('click', function () {
        $('#sidebarMenu').addClass('active');
        $('.overlaySideBar').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
        $('body').css("overflow", "hidden");
    });
});

