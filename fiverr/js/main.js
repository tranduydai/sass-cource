/* xử lý cho phần header */
testWindowScroll();
function testWindowScroll() {
    window.onscroll = function () {
        if (window.innerWidth > 600) {
            changeScrollWindow();
        }
    }
}

function changeScrollWindow() {

    if (document.body.scrollTop > 150 || document.documentElement.scrollTop > 150) {
        // screen width > 150
        document.querySelector("header").className = "";
        document.querySelector(".header__bottom").className = "header__bottom header__bottom-visible";
    } else if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
        // 10 < screen width < 150
        document.querySelector("header").className = "header--formHidden";
        document.querySelector(".header__bottom").className = "header__bottom";
    } else {
        // screen width < 10
        document.querySelector("header").className = "header--bgTransparent header--formHidden";
    }
}

/* xử lý cho phần footer bằng jquery */
$(document).ready(function () {
    var widthScreen = $(window).width();
    clickFooterItemMobile(widthScreen);

});

function clickFooterItemMobile(widthScreen) {
    // if(widthScreen <= 600) {
    $("footer .footer__items h2").on("click", function () {
        $(this).toggleClass("active");
    });
    // }
}

/* xử lý cho phần header__bottom */
var DISTANCE = 200;
var listButton = document.querySelectorAll(".header__bottom > button");
var tagUl = document.querySelector(".header__bottom > ul");
var left;
var flagPrev;
var distanceMax;
changeHeaderBottom();
window.addEventListener("resize", function () {
    changeHeaderBottom()
});

function changeHeaderBottom() {
    left = 0;
    distanceMax = 1366 - window.innerWidth - 64;

    tagUl.style.left = "0px";
    listButton[0].classList.remove("active");
    listButton[1].classList.remove("active");
    if (window.innerWidth < 1283) {
        listButton[1].classList.add("active");
    }

    // xử lý sự kiện click button
    // dùng element.onclick = function(){} cũng được
    listButton[1].addEventListener("click", function(event){
        clickButtonNext();
        event.stopImmediatePropagation();
    }, false);

    listButton[0].addEventListener("click", function(event){
        clickButtonPrev();
        event.stopImmediatePropagation();
    }, false);
}

function clickButtonNext() {
    left = left - DISTANCE;
    if (-left > distanceMax) {
        left = -distanceMax;
        listButton[1].classList.remove("active");
    }
    listButton[0].classList.add("active");
    tagUl.style.left = left + "px";

    showDropdownMenu(left + 2.4375);
}

function clickButtonPrev() {
    left += DISTANCE;
    if (left > 0) {
        left = 0;
        listButton[0].classList.remove("active");
        // flagPrev = false;
    }
    listButton[1].classList.add("active");
    tagUl.style.left = left + "px";

    showDropdownMenu(left + 2.4375);
}

function showDropdownMenu(distanceLeft = 0) {
    const PADDING_LEFT_SLIDE_BAR = 30;
    const WIDTH_COL = 250;
    const COL_GAP = 72;
    const PADDING_LEFT_CONTENT = 32;
    var maxHeight = $(window).height() - 125;
    var maxWidth = $(window).width();
    var maxCol = 1;
    for(var i = 1; i <= 4; i++){
        if((250 * i + 72 * (i - 1)) < maxWidth) {
            maxCol = i;
        }
    }
    if(maxWidth > 600) {
        $('.header__bottom__dropdown').each(function(index, val) {
            var heightDiv = $(this).height();
            var col = Math.ceil(heightDiv * 2 / maxHeight);  
            // var left = $(this).parent().offset().left;
            // console.log($(this).parent().position());
            var left = $(this).parent().position().left + distanceLeft + PADDING_LEFT_SLIDE_BAR;
            var right = $(this).parent().width() + left;
            var widthMaxLeft = right;
            var widthMaxRight = maxWidth - left;
            if(col > maxCol) {
                col = maxCol;
            }
            if(col == 1) {
                col = 2;
            }
            
            $(this).css({
                columnCount : col,
                position: 'absolute',
                right: '-16px',
                left: 'unset'
            });

            var widthDiv = col * WIDTH_COL + (col - 1) * COL_GAP + PADDING_LEFT_CONTENT;

            if(widthDiv > widthMaxLeft && widthDiv > widthMaxRight || left < 0 || right > maxWidth) {
                $(this).css({
                    position: 'fixed',
                    left: 'unset',
                    right: 0
                });
                if(widthDiv > maxWidth) {
                    $(this).css({left: 0});
                }
            } else if (widthDiv <= widthMaxRight) {
                $(this).css({
                    right: 'unset',
                    left: '-16px',
                    position: 'absolute'
                })
            }

        });
    } else {
        $('.header__bottom__dropdown').css({
            position: 'fixed',
            left: 16,
            right: 16,
            columnCount: 2
        });
    }
}
$(window).resize(function(event) {
    showDropdownMenu();
});
$(document).ready(function(){
    showDropdownMenu();
})

// xử lý phần slidebarMenu

$(document).ready(() => {
    $('.slidebar__item-level-1, .slidebar__item-level-2').click(function(event) {
        if($(event.target).attr("data-tagDiv") == $(this)[0].className) {
            $(this).children('a').toggleClass('collapse-arrow-rotate');
        }
    });
    
})