$('.services__carousel').owlCarousel({
    loop: true,
    margin: 35,
    nav: true,
    dots: false,
    autoplayTimeout: 2000,
    responsive: {
        0: {
            items: 1
        },
        576: {
            items: 2
        },
        700: {
            items: 3
        },
        900: {
            items: 4
        },
        1160: {
            items: 5
        }
    }
});

$('.feedback__carousel').owlCarousel({
    loop: true,
    margin: 35,
    nav: true,
    dots: false,
    autoplayTimeout: 2000,
    items: 1
})

$('.projects__carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: false,
    autoplayTimeout: 2000,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        800: {
            items: 3
        },
        1060: {
            items: 4
        }
    }
});